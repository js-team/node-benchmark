# Installation
> `npm install --save @types/benchmark`

# Summary
This package contains type definitions for Benchmark (https://benchmarkjs.com).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/benchmark.

### Additional Details
 * Last updated: Sun, 28 Aug 2022 07:02:30 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Asana](https://asana.com), [Charlie Fish](https://github.com/fishcharlie), and [Blair Zajac](https://github.com/blair).
